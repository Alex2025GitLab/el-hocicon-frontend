## **_Este es el repositorio de servidor (disculpe la molestia) Nest.js_**

# Instalación

## Requerimientos del servidor

- NodeJS version 20
- Nestjs cli instalado (ejemplo de instalación)

```sh
npm install -g @nestjs/cli
```

- Docker (última versión) para el contenedor de DB Postgres
- **Puertos que deben estar libres**: 5432 y 3000

## Instalación del proyecto

1. Clonar el código fuente desde el repositorio.

```sh
$ git clone https://gitlab.com/Alex2025GitLab/el-hocicon-frontend.git
```

2. Ingresar a la carpeta.

```sh
$ cd el-hocicon-frontend
```

3. Instalar las dependencias de paquetes npm

```sh
$ npm install
```

## Configuraciones

1. Copiar archivos de configuración y modificarlos según necesidad.

```sh
$ cp .env.sample .env
```

2. Dejar el env configurado como está ya que comparte variables con el frontend

3. Una vez configurado el archivo .env se debe ejecutar el comando para el contenedor de la base de datos, ejecute la siguiente línea y espere a que se active el contenedor

```sh
$ docker compose up
```

## Inicializar la base de datos

1. Para la semilla y las entidades ejecutar la siguiente línea de código

```sh
$ npm run seed
```

## Iniciar el proyecto

#### Para desarrollo (despliegue local)

```sh
$ npm run start:dev
```
