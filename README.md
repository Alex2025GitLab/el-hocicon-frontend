## **_Este es el repositorio BACKEND de servidor (disculpe la molestia) Nest.js_**

# Repositorio del Backend

Servidor api para la conexión con la base de datos Postgres sobre el framework Nest y el ORM (TypeORM)

Este base contiene los siguientes módulos:

- User
- News

Cuenta con creación del token JWT para middleware del lado del frontend.

Se trabaja con subida de imágenes en el archivo /uploads
**se recomienda no eliminar o modificar el archivo**.

## Para la instalación

Siga los pasos de **INSTALL.md** es importante tener los puertos 5432 y 3000 disponibles
