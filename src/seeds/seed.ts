import { DataSource, DataSourceOptions } from 'typeorm';
import { runSeeders, SeederOptions } from 'typeorm-extension';
import { User } from '../users/user.entity';
import { News } from '../news/news.entity';

import { UserSeeder } from './user.seeder';
import { NewsSeeder } from './news.seeder';

const options: DataSourceOptions & SeederOptions = {
  type: 'postgres',
  host: 'localhost',
  port: 5432,
  username: 'hocicon_user',
  password: '@Hocicon_2023',
  database: 'hocicon',
  entities: [User, News],
  synchronize: true,
  seeds: [UserSeeder, NewsSeeder],
};

const dataSource = new DataSource(options);

dataSource.initialize().then(async () => {
  await dataSource.synchronize(true);
  await runSeeders(dataSource);
  console.log('Seed concluido con éxito');
  process.exit();
});
