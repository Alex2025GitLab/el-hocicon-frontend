import { Seeder } from 'typeorm-extension';
import { DataSource } from 'typeorm';
import { User } from '../users/user.entity';

export class UserSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<void> {
    await dataSource.query('TRUNCATE "user" RESTART IDENTITY;');
    {
    }
    const repository = dataSource.getRepository(User);
    await repository.insert([
      {
        email: 'aaalex2025@gmail.com',
        username: 'Alexander Nina',
        profile_img: 'a068c2a6-9119-4bba-a2a6-5279871801ba.jpg',
        password:
          '$2a$11$5yYy50s4YteXoGKXU5VeKOzDWAA2EnJNlBXk1yCYmkEy98Hg7VJsq',
      },
      {
        email: 'seed1@gmail.com',
        username: 'Eric Cheung',
        profile_img: '566b19b6-493e-4999-86be-4ca9ed4dbe74.jpg',
        password:
          '$2a$11$5yYy50s4YteXoGKXU5VeKOzDWAA2EnJNlBXk1yCYmkEy98Hg7VJsq',
      },
      {
        email: 'seed2@gmail.com',
        username: 'Wayne Cole',
        profile_img: 'bf66fbc9-2a29-45d2-a59c-9c92bdbbbca1.jpg',
        password:
          '$2a$11$5yYy50s4YteXoGKXU5VeKOzDWAA2EnJNlBXk1yCYmkEy98Hg7VJsq',
      },
      {
        email: 'seed3@gmail.com',
        username: 'Michelle Toh',
        profile_img: 'bfd676f5-cf2f-45c4-a51c-773e8f756407.jpg',
        password:
          '$2a$11$5yYy50s4YteXoGKXU5VeKOzDWAA2EnJNlBXk1yCYmkEy98Hg7VJsq',
      },
      {
        email: 'seed4@gmail.com',
        username: 'Hannah Jackson',
        profile_img: '3aa1f79a-7c43-4545-9437-b8ba84ae47c5',
        password:
          '$2a$11$5yYy50s4YteXoGKXU5VeKOzDWAA2EnJNlBXk1yCYmkEy98Hg7VJsq',
      },
    ]);
  }
}
