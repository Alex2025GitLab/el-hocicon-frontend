import { Seeder } from 'typeorm-extension';
import { DataSource } from 'typeorm';
import { News } from '../news/news.entity';

export class NewsSeeder implements Seeder {
  public async run(dataSource: DataSource): Promise<void> {
    await dataSource.query('TRUNCATE "news" RESTART IDENTITY;');
    {
    }
    const repository = dataSource.getRepository(News);
    await repository.insert([
      {
        image_url: '27639b3d-6c3b-491b-b596-b3bd9ec97821.jpg',
        content:
          'In what world is an NFL head coachs job in jeopardy after leading his team to 12 wins and playoff berths in three consecutive seasons?\r\nIn Jerrys world.\r\nThis is the precarious perch on which Cowboys… [+5263 chars]',
        description:
          'Cowboys owner Jerry Jones is 81 years old and he wants results now. Postseason results.',
        title:
          "Jerry Jones should fire Mike McCarthy after Cowboys' flop as Bill Belichick speculation grows",
        place_publication: 'New York Post',
        created_by: 1,
        updated_by: 1,
      },
      {
        image_url: '524144c3-67e5-4086-9015-c6f4f60b4e49.jpg',
        content:
          '"PERRY, Iowa —The principal wounded in the Perry, Iowa, school shooting on Jan. 4 has died. \r\nPerry High School Principal Dan Marburger and six others, including two staff members and four students, w… [+4547 chars]',
        description:
          '"Dan was a tremendous leader in our school district and a loving husband, father, and grandfather. Our school community is heartbroken by Dan’s death," officials with the school district said.',
        title:
          'Principal Dan Marburger, wounded in Perry, Iowa school shooting, has died',
        place_publication: 'KCCI Des Moines',
        created_by: 1,
        updated_by: 1,
      },
      {
        image_url: '32473440-0c66-4b15-ad64-25459ed092ba.jpg',
        content:
          'Enjoy the hottest mobile tech storylines, delivered straight to your inbox.',
        description:
          'While Samsung was busy reprimanding leakers for leaks, a retailer sneakily put up the Galaxy S24 for sale.',
        title:
          'Forget leaks, a retailer is already selling Galaxy S24 apparently but warns of limited stock',
        place_publication: 'PhoneArena',
        created_by: 2,
        updated_by: 2,
      },
      {
        image_url: '362e7b7f-da59-45ed-acf2-121fc24d0a76.jpg',
        content:
          'Graham Nash and his wife lost power to their home near Forest Heights around 9:30 a.m. Saturday.\r\nMore than a day later, he was still waiting for Portland General Electric to restore it, and many oth… [+7707 chars]',
        description:
          'Day 3 of a winter storm left many Portland area residents without heat and electricity. Here’s how they’re coping.',

        title:
          'Portland area residents take ‘wait and freeze’ approach to power outages',
        place_publication: 'OregonLive',
        created_by: 3,
        updated_by: 3,
      },
      {
        image_url: '68b4f382-2999-45b3-903c-1359753671c0.jpg',
        content:
          'BROWNSVILLE, Texas (AP) After Texas fenced off a park along the U.S.-Mexico border and began turning away Border Patrol agents, Republican Gov. Greg Abbott explained why at a campaign stop near Houst… [+5495 chars]',
        description:
          'The White House says U.S. Border Patrol agents need access to the border after officials say three migrants drowned while trying to cross the Rio Grande near the Texas border city of Eagle Pass. Texas officials have recently begun blocking Border Patrol agent…',

        title:
          'Migrant deaths in Rio Grande intensify tensions between Texas, Biden administration over crossings',

        place_publication: 'Associated Press',
        created_by: 2,
        updated_by: 2,
      },
      {
        image_url: '394f7528-9d3d-468b-a9ef-f295e049fb0b.jpg',
        content:
          'Tonight marks the 29th annual Critics Choice Awards, celebrating achievements in both film and TV, as voted on by print, broadcast, and digital journalists who cover entertainment. Chelsea Handler is… [+1649 chars]',
        description:
          'See all of the looks from the red carpet and check back in real time for live updates throughout the night.',

        title: 'Critics Choice Awards 2024: Fashion—Live From the Red Carpet',

        place_publication: 'Vogue',
        created_by: 4,
        updated_by: 4,
      },
      {
        image_url: '42ebfe6f-ac5c-44bf-902f-3796bb3ee97b.jpg',
        content:
          'Yemens Houthi rebels fired an anti-ship cruise missile toward an American destroyer in the Red Sea on Sunday, but a US fighter jet shot it down in the latest attack roiling global shipping amid Israe… [+4247 chars]',
        description:
          'Yemen’s Houthi rebels fired an anti-ship cruise missile toward an American destroyer in the Red Sea on Sunday, but a US fighter jet shot it down in the latest attack roiling global shipping amid Is…',

        title:
          'Yemen Houthi rebels fire missile at US warship in Red Sea in first attack after American-led strike ',
        place_publication: 'New York Post',
        created_by: 2,
        updated_by: 2,
      },
      {
        image_url: '35413023-de6b-4cbd-b4b9-c57a21b7e13f.jpg',
        content:
          'Yemens Houthi rebels fired an anti-ship cruise missile toward an American destroyer in the Red Sea on Sunday, but a US fighter jet shot it down in the latest attack roiling global shipping amid Israe… [+4247 chars]',
        description:
          'Yemen’s Houthi rebels fired an anti-ship cruise missile toward an American destroyer in the Red Sea on Sunday, but a US fighter jet shot it down in the latest attack roiling global shipping amid Is…',

        title:
          'What we learned from Super Wild Card weekend, Day 2: Dak Prescott, Cowboys remain frauds ',
        place_publication: 'New York Post',
        created_by: 4,
        updated_by: 4,
      },
    ]);
  }
}
