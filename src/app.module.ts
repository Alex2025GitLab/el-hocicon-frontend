import { Module } from '@nestjs/common';
import { UsersModule } from './users/users.module';
import { NewsModule } from './news/news.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';
import { Poll } from './poll/poll.entity';
import { PollController } from './poll/poll.controller';
import { PollService } from './poll/poll.service';
import { AuthModule } from './auth/auth.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { LandingModule } from './landing/landing.module';
import { dataSourceOptions } from './data-source';

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: '.env' }),
    ServeStaticModule.forRoot({ rootPath: join(__dirname, '../', 'uploads') }),
    TypeOrmModule.forRoot(dataSourceOptions),
    TypeOrmModule.forFeature([Poll]),
    UsersModule,
    NewsModule,
    AuthModule,
    LandingModule,
  ],
  controllers: [PollController],
  providers: [PollService],
})
export class AppModule {}
