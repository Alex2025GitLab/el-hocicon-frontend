import { Module } from '@nestjs/common';
import { NewsController } from './news.controller';
import { NewsService } from './news.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { News } from './news.entity';
import { ConfigModule } from '@nestjs/config';
import { User } from '../users/user.entity';

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: '.env' }),
    TypeOrmModule.forFeature([News, User]),
  ],
  controllers: [NewsController],
  providers: [NewsService],
})
export class NewsModule {}
