import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class NewsUploadDto {
  @IsNotEmpty()
  @IsString()
  title: string;

  @IsString()
  placePublication: string;

  @IsString()
  description: string;
  @IsString()
  content: string;

  @IsNumber()
  idUser: number;
}

export class NewsEditDto {
  title?: string;
  placePublication?: string;
  description?: string;
  content?: string;
  @IsNotEmpty()
  @IsNumber()
  idUser: number;
}
