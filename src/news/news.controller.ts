import {
  Controller,
  UploadedFile,
  UseInterceptors,
  Post,
  Get,
  Body,
  ParseFilePipeBuilder,
  HttpStatus,
  Delete,
  Param,
  ParseIntPipe,
  Patch,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { NewsEditDto, NewsUploadDto } from './news.dto';
import { diskStorage } from 'multer';
import { v4 as uuidv4 } from 'uuid';
import { NewsService } from './news.service';
// import { decode } from 'jsonwebtoken';
// import { Request } from 'express';

@Controller('news')
export class NewsController {
  constructor(private newsService: NewsService) {}
  // Imagen de 2 MB y JPG
  @Post()
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: function (req, file, cb) {
          cb(null, uuidv4() + '.jpg');
        },
      }),
    }),
  )
  public async updloadFile(
    @Body() body: NewsUploadDto,
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType: 'image/jpeg',
        })
        .addMaxSizeValidator({ maxSize: 2 * 1024 * 1024 })
        .build({
          fileIsRequired: false,
          errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
        }),
    )
    file: Express.Multer.File,
  ) {
    // console.log(request);
    // const jwtToken = request.headers.cookie;
    // console.log(jwtToken);
    // if (
    //   jwtToken &&
    //   jwtToken.length > 0 &&
    //   jwtToken.includes(process.env.TOKEN)
    // ) {
    //   const payloadToken = jwtToken.split('=')[1];
    //   const dataPayload = decode(payloadToken);
    //   // @ts-expect-error: Sub es el id de usuario actual
    //   const idUser: number = parseInt(dataPayload.sub);
    //   return this.newsService.createNew(
    //     body,
    //     file ? file.filename : '',
    //     idUser,
    //   );
    // } else {
    //   return new HttpException('Acceso denegado', HttpStatus.FORBIDDEN);
    // }
    return this.newsService.createNew(body, file ? file.filename : '');
  }

  @Get()
  getNews() {
    // console.log(request);
    // const jwtToken = request.headers.cookie;
    // if (jwtToken.length > 0 && jwtToken.includes(process.env.TOKEN)) {
    //   console.log(jwtToken);
    //   return this.newsService.getNews();
    // } else {
    //   return new HttpException('Acceso denegado', HttpStatus.FORBIDDEN);
    // }
    return this.newsService.getNews();
  }

  @Delete(':id')
  deleteUser(@Param('id', ParseIntPipe) id: number) {
    return this.newsService.deleteNew(id);
  }

  @Patch(':id')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads',
        filename: function (req, file, cb) {
          cb(null, uuidv4() + '.jpg');
        },
      }),
    }),
  )
  async updateUser(
    @Param('id', ParseIntPipe) id: number,
    @Body() body: NewsEditDto,
    @UploadedFile(
      new ParseFilePipeBuilder()
        .addFileTypeValidator({
          fileType: 'image/jpeg',
        })
        .addMaxSizeValidator({ maxSize: 2 * 1024 * 1024 })
        .build({
          fileIsRequired: false,
          errorHttpStatusCode: HttpStatus.UNPROCESSABLE_ENTITY,
        }),
    )
    file: Express.Multer.File,
  ) {
    return this.newsService.updateNew(id, body, file ? file.filename : '');
  }
}
