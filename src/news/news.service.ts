import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { News } from './news.entity';
import { Repository } from 'typeorm';
import { NewsEditDto, NewsUploadDto } from './news.dto';
import { User } from '../users/user.entity';
// import * as fs from 'fs';

@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(News) private readonly newsRespository: Repository<News>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}

  async createNew(article: NewsUploadDto, file: string) {
    const newArticle = this.newsRespository.create({
      content: article.content,
      created_by: article.idUser,
      image_url: file,
      description: article.description,
      place_publication: article.placePublication,
      title: article.title,
      updated_by: article.idUser,
    });
    const savedArticle = this.newsRespository.save(newArticle);
    return {
      ...savedArticle,
      success: 'Noticia creada, actualice la página para ver los cambios',
    };
  }

  async deleteNew(id: number) {
    const newFound = await this.newsRespository.findOne({
      where: { id },
    });
    if (!newFound) {
      return new HttpException('Noticia no encontrada.', HttpStatus.NOT_FOUND);
    }
    // Eliminar imagen
    // await fs.unlink();
    const deletedNew = this.newsRespository.delete({ id });

    return {
      ...deletedNew,
      success: 'Noticia eliminada, actualice la página para ver los cambios',
    };
  }

  async updateNew(id: number, article: NewsEditDto, file: string) {
    const newsFound = await this.newsRespository.findOne({
      where: {
        id: id,
      },
    });
    if (!newsFound) {
      return new HttpException('Noticia no encontrada', HttpStatus.NOT_FOUND);
    }

    // Eliminar imagen si existe file
    if (file.length > 0) {
    }

    const updatedData: News = {
      id,
      content: article.content ? article.content : newsFound.content,
      description: article.description
        ? article.description
        : newsFound.description,
      image_url: file.length > 0 ? file : newsFound.image_url,
      place_publication: article.placePublication
        ? article.placePublication
        : newsFound.place_publication,
      title: article.title ? article.title : newsFound.title,
      updated_by: article.idUser,
      created_at: newsFound.created_at,
      created_by: newsFound.created_by,
      updated_at: newsFound.updated_at,
    };

    return this.newsRespository.save(updatedData);
  }

  async getNews() {
    const allUsers = await this.userRepository.find({
      select: {
        id: true,
        email: true,
        username: true,
        profile_img: true,
      },
    });
    const allNews = await this.newsRespository.find();

    const mappedNews = [];
    for (const arNew of allNews) {
      const nombreCreador = allUsers.find(
        (elem) => elem.id === arNew.created_by,
      );
      const nombreEditor = allUsers.find(
        (elem) => elem.id === arNew.updated_by,
      );
      mappedNews.push({
        ...arNew,
        created_by: nombreCreador.username,
        updated_by: nombreEditor.username,
      });
    }
    return {
      users: allUsers,
      news: mappedNews,
    };
  }
}
