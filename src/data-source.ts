import { config } from 'dotenv';
import { DataSourceOptions } from 'typeorm';
import { User } from './users/user.entity';
import { News } from './news/news.entity';
import { Poll } from './poll/poll.entity';
import { SeederOptions } from 'typeorm-extension';

config();

export const dataSourceOptions: DataSourceOptions & SeederOptions = {
  type: 'postgres',
  host: process.env.DB_URL,
  port: parseInt(process.env.DB_PORT),
  username: process.env.DB_USER,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_NAME,
  entities: [Poll, User, News],
  synchronize: true,
  seeds: ['dist/db/seeds/**/*.js'],
};
