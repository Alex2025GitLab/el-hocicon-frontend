import { Body, Controller, Get, Post } from '@nestjs/common';
import { LandingService } from './landing.service';

@Controller('landing')
export class LandingController {
  constructor(private landingService: LandingService) {}

  @Get()
  getLanding() {
    return this.landingService.getNews();
  }

  @Post()
  searchNews(@Body() query) {
    return this.landingService.searchNews(query.q);
  }
}
