import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { News } from '../news/news.entity';
import { User } from '../users/user.entity';
import { Like, Repository } from 'typeorm';
import { LandingDto, NewsForLanding } from './landing.dto';

@Injectable()
export class LandingService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    @InjectRepository(News) private readonly newsRepository: Repository<News>,
  ) {}
  async getNews(): Promise<LandingDto> {
    const allNews = await this.newsRepository.find();
    const allUsers = await this.userRepository.find({
      select: {
        id: true,
        username: true,
        profile_img: true,
      },
    });

    const groupedNews: NewsForLanding[] = [];
    for (const art of allNews) {
      const authorData = allUsers.find((elem) => elem.id === art.created_by);

      groupedNews.push({
        authorImg: authorData.profile_img,
        author: authorData.username,
        description: art.description,
        id: art.id,
        imageUrl: art.image_url,
        placePublication: art.place_publication,
        publishedAt: art.created_at,
        title: art.title,
      });
    }

    return {
      latest: groupedNews.length > 0 ? groupedNews[0] : undefined,
      latestNews: groupedNews,
    };
  }
  async searchNews(query: string) {
    const allUsers = await this.userRepository.find({
      select: {
        id: true,
        username: true,
        profile_img: true,
      },
    });

    const searchQuery = `%${query}%`;
    const data = await this.newsRepository.find({
      where: [
        {
          title: Like(searchQuery),
        },
        { description: Like(searchQuery) },
        { content: Like(searchQuery) },
      ],
    });

    const groupedNews: NewsForLanding[] = [];
    for (const art of data) {
      const authorData = allUsers.find((elem) => elem.id === art.created_by);

      groupedNews.push({
        authorImg: authorData.profile_img,
        author: authorData.username,
        description: art.description,
        id: art.id,
        imageUrl: art.image_url,
        placePublication: art.place_publication,
        publishedAt: art.created_at,
        title: art.title,
      });
    }

    return { news: groupedNews, success: 'Resultados encontrados' };
  }
}
