export class NewsForLanding {
  id: number;
  title: string;
  imageUrl: string;
  publishedAt: string | Date;
  placePublication: string;
  author: string;
  description: string;
  authorImg: string;
}
export class LandingDto {
  latest: NewsForLanding | undefined;
  latestNews: NewsForLanding[];
}
