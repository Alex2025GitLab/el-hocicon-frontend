import {
  IsEmail,
  IsNotEmpty,
  IsString,
  Matches,
  MinLength,
} from 'class-validator';

export class CreateUserDto {
  @IsNotEmpty()
  @IsEmail()
  @IsString()
  email: string;

  @IsNotEmpty()
  username: string;

  @IsNotEmpty()
  @MinLength(8)
  @Matches(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}$/, {
    message:
      'Debe contener al menos 1 (número, letra minúscula, letra mayúscula)',
  })
  password: string;
}

export class UpdateUserDto {
  username?: string;

  @Matches(/^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z]).{8,32}$/, {
    message:
      'Debe contener al menos 1 (número, letra minúscula, letra mayúscula)',
  })
  password?: string;
}
