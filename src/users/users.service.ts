import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from './user.entity';
import * as bcrypt from 'bcryptjs';
import { Repository } from 'typeorm';
import { CreateUserDto, UpdateUserDto } from './user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}

  async createUser(user: CreateUserDto, file: string) {
    const userFound = await this.userRepository.findOne({
      where: {
        email: user.email,
      },
    });
    if (userFound) {
      return new HttpException(
        'Email ya existe en la base de datos.',
        HttpStatus.CONFLICT,
      );
    }
    const hashedPassword = await bcrypt.hash(user.password, 11);
    console.log(hashedPassword);
    const newUser = this.userRepository.create({
      email: user.email,
      password: hashedPassword,
      username: user.username,
      profile_img: file,
    });
    return this.userRepository.save(newUser);
  }

  getUsers() {
    return this.userRepository.find();
  }

  async getUser(id: number) {
    const userFound = await this.userRepository.findOne({
      where: {
        id: id,
      },
    });
    if (!userFound) {
      return new HttpException('Usurio no encontrado.', HttpStatus.NOT_FOUND);
    }
    return userFound;
  }

  async deleteUser(id: number) {
    const userFound = await this.userRepository.findOne({ where: { id } });
    if (!userFound) {
      return new HttpException('Usurio no encontrado.', HttpStatus.NOT_FOUND);
    }
    return this.userRepository.delete({ id });
  }

  updateUser(id: number, user: UpdateUserDto) {
    return this.userRepository.update({ id }, user);
  }
}
