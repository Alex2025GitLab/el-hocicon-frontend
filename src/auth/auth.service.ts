import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { User } from '../users/user.entity';
import { Repository } from 'typeorm';
import { AuthLoginDto } from './auth.dto';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcryptjs';
import { InjectRepository } from '@nestjs/typeorm';

@Injectable()
export class AuthService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private jwtService: JwtService,
  ) {}

  async signIn(loginData: AuthLoginDto) {
    // Verificar si la cuenta existe
    const findedUser = await this.userRepository.findOne({
      where: { email: loginData.email },
    });
    if (!findedUser) {
      return new HttpException('Cuenta no existe', HttpStatus.NOT_FOUND);
    }

    // Comparar con contraseña hashed
    if (await bcrypt.compare(loginData.password, findedUser.password)) {
      const jwtPayload = { sub: findedUser.id, username: findedUser.username };
      return {
        status: HttpStatus.OK,
        access_token: await this.jwtService.signAsync(jwtPayload),
        user: findedUser.id,
      };
    } else {
      return new HttpException('Contraseña incorrecta', HttpStatus.BAD_REQUEST);
    }
  }
}
